import styles from '../styles/movieCard.module.scss'

import { useState, useEffect } from 'react'

import Button from '@material-ui/core/Button';

import {useRouter} from 'next/router'

import * as gtag from '../lib/gtag'

export default function MovieCard({movie, personName}) {

  const router = useRouter()

  const {content} = movie;

  const [imgPosterUrlTMDB, setImgPosterUrlTMDB] = useState(null);
  const [imgBackDropUrlTMDB, setImgBackDropUrlTMDB] = useState(null);
  const [genresFromTMDB, setGenresFromTMDB] = useState(null);
  const [voteAverageFromTMDB, setVoteAverageFromTMDB] = useState(null);
  const [voteCountFromTMDB, setVoteCountFromTMDB] = useState(null);

  const genresTMDB = {
    "12": "Aventure",
    "14":"Fantastique",
    "16":"Animation",
    "18":"Drame",
    "27":"Horreur",
    "28": "Action",
    "35":"Comédie",
    "36":"Histoire",
    "37":"Western",
    "53":"Thriller",
    "80":"Crime",
    "99":"Documentaire",
    "878":"Science-Fiction",
    "9648":"Mystère",
    "10402":"Musique",
    "10751":"Familial",
    "10749":"Romance",
    "10752":"Guerre",
    "10770":"Téléfilm"
  }

  useEffect(async () => {
    var res = await getImageUrlFromApi(content.original_title);
    if (!res){ res = await getImageUrlFromApi(content.product_title)};
  }, [])

/* use TMDB API to get film covers, dropback, genres, vote_average and vote_count */
  async function getFilmsFromTMDB(filmName){
    return fetch('https://api.themoviedb.org/3/search/movie?api_key=' + process.env.tmdbApiToken + '&language=fr&query=' + filmName)
      .then((res) => res.json())
      .catch((error) => console.error(error))
  }
  async function getImageUrlFromApi (filmName) {
    var res = await getFilmsFromTMDB(filmName)
    if (res.hasOwnProperty('results')){
      if (res.results.length > 0){
        if (res.results[0].poster_path){setImgPosterUrlTMDB('https://image.tmdb.org/t/p/w300' + res.results[0].poster_path)};
        if (res.results[0].backdrop_path){setImgBackDropUrlTMDB('https://image.tmdb.org/t/p/w300' + res.results[0].backdrop_path)};
        setGenresFromTMDB(res.results[0].genre_ids)
        setVoteAverageFromTMDB(res.results[0].vote_average);
        setVoteCountFromTMDB(res.results[0].vote_count);
        return true
      }
    }
    return false
  }
  function displayPosterImgTMDB(){
    if (imgPosterUrlTMDB){
      return (
        <img
            src={imgPosterUrlTMDB}
            alt={"TMDB poster of the movie " + movie.title}
            className={styles.card_img}
            importance={'high'}
          />
      )
    }
    return (<></>)
  }


  function displayBackDropImgTMDB(){
    if (imgBackDropUrlTMDB){
      return (
        <img
            src={imgBackDropUrlTMDB}
            alt={"TMDB backdrop of the movie " + movie.title}
            className={styles.card_img}
            importance={'high'}
          />
      )
    }
    return (<></>)
  }
  function displayGenres(){
    if (genresFromTMDB){
      return (
        <div style={{marginTop:5, marginBottom:5, fontSize:12, textAlign:'center'}}>
          {
            genresFromTMDB.map((genreId, i) => {
              if (i==0){
                return (genresTMDB[genreId])
              }
              else{
                return (', ' + genresTMDB[genreId])
              }
            })
          }
        </div>
      )
    }
  }

  const searchTrailer = (filmName) => {
    gtag.event({
      action: "search_trailer",
      category: "trailers",
      label: "Search Trailer",
      value: filmName
    })
    router.push("https://www.google.com/search?q=" + content.search_engine);
  }

  const searchImdb = (filmName) => {
    gtag.event({
      action: "search_imdb",
      category: "imdb",
      label: "Search imdb",
      value: filmName
    })
    router.push('https://www.imdb.comp/title/'+content.imdb_id);
  }


  return (
    <div className={styles.card}>
      <div className={styles.card_img_container}>
        {displayPosterImgTMDB()}
        {displayBackDropImgTMDB()}
        {(voteAverageFromTMDB)?'Note '+ voteAverageFromTMDB:<></>}
        {(voteAverageFromTMDB)?' / '+ voteCountFromTMDB + ' votes':<></>}
        {displayGenres()}
        {/* API pictures urls furnished (i.e. 'http://fr.image-1.filmtrailer.com/119886.jpg') are not working
          (isAmp)?
          content.pictures.map((picture,i) => (
            <amp-img
              key={i}
              src={content.pictures[0].content.url}
              alt={(i==0)?"Main picture of the movie " + movie.title:"Picture " + i.toString() + " of the movie " + movie.title}
              width='100'
              height='100'
              layout='fixed'
              className={styles.card_img}
            />
          ))
          :
          content.pictures.map((picture,i) => (
            <img
              key={i}
              src={content.pictures[0].content.url}
              alt={(i==0)?"Main picture of the movie " + movie.title:"Picture " + i.toString() + " of the movie " + movie.title}
              className={styles.card_img}
              importance={(i==0)?'high':'normal'}
            />
          ))
        */}
      </div>
      <div className={styles.card_info}>
        <h1 className={styles.h1}>{content.product_title}</h1>
        <div style={{marginBottom:10}}>
          <div className={styles.text_info}>
            Pour ce film, <b>{personName}</b> est: {' '}
            <b>
            { content.fonctions.map((fct) => (
              fct.content.libelle + ' '
            ))}
            </b>
          </div>
        </div>
        <div style={{marginBottom:10}}>
          <div className={styles.text_info}><b><u>titre original:</u></b> {content.original_title}</div>
          <div className={styles.text_info}>
            <b><u>site officiel:</u> </b>
            <a className={styles.link} href={content.official_website}>{content.official_website}</a>
          </div>
        </div>

        <div style={{marginBottom:10}}>
          <div className={styles.text_info}><b><u>année de production:</u></b> {content.production_year}</div>
          <div className={styles.text_info}><b><u>premiere:</u></b> {content.premiere}</div>
        </div>
        <div style={{marginBottom:10}}>
          <div className={styles.text_info}><b><u>durée du film:</u></b> {Math.trunc(content.movie_duration/60)}H{content.movie_duration%60} </div>
          {(content.age_limit!=null)?<div className={styles.text_info}><b><u>age recommandé:</u></b> {content.age_limit}</div>:<></>}
        </div>
        <p className={styles.text_justify}><b><u>Description:</u></b> {content.description}</p>

        <div style={{flex:1, margin:10, marginTop:0}}>
          <Button  variant="contained" color="primary" onClick={() => searchImdb(content.product_title, content.imdb_id)} >
            Voir plus d'informations sur imdb
          </Button>
        </div>
        <div style={{flex:1, margin:10, marginTop:0}}>
          <Button variant="contained" color="primary" onClick={() => searchTrailer(content.product_title, content.search_engine)}>
            Recherchez la bande annonce sur google
          </Button>
        </div>
{/*     Disable for now as we have not yet create the api token to brightcover (trial account to create)
        <p><u>Bande-annonce sur brightcove: </u></p>
        {(isAmp)?<></>:
          <div style={{flex:1,maxHeight:360, marginBottom:10}}>
            <iframe src={"https://players.brightcove.net/" + content.brightcove_id + "/default_default/index.html?videoId=ref:skivideo&muted"}
              allowFullScreen
              webkitallowfullscreen="true"
              mozallowfullscreen="true"
              width="100%"
              height="100%">
            </iframe>
          </div>
        }
*/}
      </div>
    </div>
  )

}
