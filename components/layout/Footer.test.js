import React from 'react';
import { render, screen } from '@testing-library/react';
import Footer from './Footer'

test('Footer component is rendering', () => {

  render(<Footer/>);

  expect(screen.getByText('MENTIONS LEGALES')).toBeInTheDocument();
});
