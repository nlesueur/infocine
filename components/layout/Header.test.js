import React from 'react';
import { render, screen } from '@testing-library/react';
import Header from './Header'

test('Header component is rendering', () => {

  render(<Header/>);

  expect(screen.getByText('Info Ciné')).toBeInTheDocument();
});
