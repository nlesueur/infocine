import React, { Component } from 'react';

import styles from '../../styles/footer.module.scss'

class Footer extends Component {
  render () {
    return (
      <div>
        <div className={styles.line2}/>
        <div className={styles.line1}/>
        <div className={styles.footer}>
          <div className={styles.itemLeft}>
            <a
              href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
              target="_blank"
              rel="noopener noreferrer"
              className={styles.mentions}
            >
              Powered by{' '}
              <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
            </a>
          </div>
          <div className={styles.itemRight}>
            <a href="/mentionsLegales" className={styles.mentions}>
              <h2>MENTIONS LEGALES</h2>
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default Footer
