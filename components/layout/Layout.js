import React, { Component } from 'react';
import Header from './Header';
import Footer from './Footer';
import Head from 'next/head';

function Layout(props){

  const { children } = props

  return (
    <div>
      <Header/>
      <div style={{minHeight:550}}>
        {children}
      </div>
      <Footer/>
    </div>
  );

}

export default Layout
