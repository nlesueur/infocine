import React from 'react';
import { render, screen } from '@testing-library/react';
import PersonCard from './PersonCard'

test('PersonCard component is rendering', () => {
  let personData = require('./PersonCard.test.json');

  render(<PersonCard person={personData}  />);

  expect(screen.getByText('Liam Neeson')).toBeInTheDocument();
});
