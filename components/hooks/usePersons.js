import React, { useState, useEffect } from 'react';

//Custom hook to handle api call and data fetching for infiniteScroll component

function usePersons(personsResult, apiEndPoint) {

  const {persons, offset} = personsResult

  const [items, setItems] = useState(persons);
  const [nextOffset, setNextOffset] = useState(offset);

  async function fetchMoreData() {
    const res = await fetch(apiEndPoint + '/' + nextOffset)
    if (res.status == 200) {
      const result = await res.json()
      if (result.hasOwnProperty('persons')){
        setItems(items.concat(result.persons))
      }
      else{
        console.log('error while fetching more data')
      }
      if (result.hasOwnProperty('offset')){
        setNextOffset(result.offset)
      }
      else{
        setNextOffset(null)
      }
    }
    else{
      console.log('error while fetching more data')
    }
  }

  return {items, nextOffset, fetchMoreData}
}

export default usePersons
