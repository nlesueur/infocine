import styles from '../styles/personCard.module.scss'

import { useRouter } from 'next/router'

import { useState, useEffect } from 'react'

export default function PersonCard({person}) {

  const router = useRouter()
 const [isDetailsAvailable, setIsDetailsAvailable] = useState(false);

  useEffect(async () => {
    const res = await fetch(process.env.baseUrl + "/fiches/" + person.path)
    if (res.status !== 200) { return; }
    setIsDetailsAvailable(true);
  }, [])


  const handleClick = () => {
    if (isDetailsAvailable){
      router.push("/fiches/" + person.path);
    }
  }

  return (
    <div style={{}} onClick={handleClick} className={[styles.card, isDetailsAvailable?'':styles.disablePointer].join(" ")}>
      <div className={styles.card_img_container}>
        <img
          src={process.env.imagesApiEndPoint +  person.img}
          alt={"Picture of " + person.fullname}
          className={styles.card_img}
          importance={'high'}
        />
      </div>
      <div className={styles.card_info}>
        <h1 className={styles.h1}>{person.fullname}</h1>
        <div className={styles.text_info}><b><u>date de naissance:</u></b> {person.date_naissance}</div>
        <div className={styles.text_info}><b><u>profession:</u></b> {person.profession}</div>
        <div className={styles.text_info}><b><u>dbPedia:</u></b> <a className={styles.link} href={person.urldbpedia}>{person.urldbpedia}</a></div>
        <p className={styles.text_justify}><b><u>Biographie:</u></b> {person.commentaire}</p>
      </div>
    </div>

  )

}
