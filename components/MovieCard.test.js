import React from 'react';
import { render, screen } from '@testing-library/react';
import MovieCard from './MovieCard'

test('MovieCard component is rendering', () => {
  let movieData = require('./MovieCard.test.json');

  render(<MovieCard movie={movieData}  />);

  expect(screen.getByText('Le choc des titans')).toBeInTheDocument();
});
