const { PHASE_DEVELOPMENT_SERVER } = require('next/constants')

module.exports = (phase, { defaultConfig }) => {
  if (phase === PHASE_DEVELOPMENT_SERVER) {
    return {

//      async rewrites() {
//        return [
//          {
//            source: '/sitemap.xml',
//            destination: '/api/sitemap'
//          }
//        ]
//      },

      //Specific development configuration
      images: {
        domains: ['lpnt.fr','ba-api.lpnt.fr'],
      },
      env: {
        env:'dev',
        baseUrl:'http://localhost:3000',
        fetchMoreActorsApi:'/api/acteurs',

        homeApiEndPoint:'https://ba-api.lpnt.fr/rubrique/home',
        actorsApiEndPoint:'https://ba-api.lpnt.fr/rubrique/acteurs',
        directorsApiEndPoint:'https://ba-api.lpnt.fr/rubrique/directeurs',
        productorsApiEndPoint:'https://ba-api.lpnt.fr/rubrique/producteurs',
        fichesApiEndPoint:'https://ba-api.lpnt.fr/personne',
        imagesApiEndPoint:'https://ba-api.lpnt.fr/images/personne/',

        tmdbApiToken:'eeb397c5f6b57138a57d73d219370bb9',
        analyticsToken:'G-S012B0E07M'
      }
    }
  }

  return {

//    async rewrites() {
//      return [
//        {
//          source: '/sitemap.xml',
//          destination: '/api/sitemap'
//        }
//      ]
//    },

    //Default configuration
    images: {
      domains: ['lpnt.fr','ba-api.lpnt.fr'],
    },
    env: {
      env:'prod',
      baseUrl:'https://infocine.vercel.app',
      fetchMoreActorsApi:'/api/acteurs',
      homeApiEndPoint:'https://ba-api.lpnt.fr/rubrique/home',
      actorsApiEndPoint:'https://ba-api.lpnt.fr/rubrique/acteurs',
      directorsApiEndPoint:'https://ba-api.lpnt.fr/rubrique/directeurs',
      productorsApiEndPoint:'https://ba-api.lpnt.fr/rubrique/producteurs',
      fichesApiEndPoint:'https://ba-api.lpnt.fr/personne',
      imagesApiEndPoint:'https://ba-api.lpnt.fr/images/personne/',
      tmdbApiToken:'eeb397c5f6b57138a57d73d219370bb9',
      analyticsToken:'G-S012B0E07M'
    }
  }
}
