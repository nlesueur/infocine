module.exports = {
  collectCoverageFrom: [
    '**/components/*.{js,jsx}',
    '**/pages/*.{js,jsx}',
    '**/pages/api*.{js,jsx}',
    '**/pages/fiches*.{js,jsx}',
    '!**/.git/**',
    '!**/.next/**',
    '!**/coverage/**',
    '!**/node_modules/**',
    '!**/public/**',
    '!**/styles/**',
    '!**/tests/**',
    '!jest.config.js',
    '!next-seo.config.js',
    '!next.config.js',
  ],
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100,
    },
  },
  moduleNameMapper: {
    "^.+\\.(css|scss)$": "identity-obj-proxy",
//    '\\.(scss)$': 'babel-jest',
//    '\\.(css|less)$': '<rootDir>/__mocks__/styleMock.js',
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/__mocks__/fileMock.js',
  },
  setupFiles: [
    '<rootDir>/tests/setup.js',
  ],
  setupFilesAfterEnv: [
    '@testing-library/jest-dom/extend-expect',
    '<rootDir>/tests/setupAfterEnv.js',
  ],
  testMatch: [
    '**/?(*.)+(spec|test).[jt]s?(x)',
  ],
  testPathIgnorePatterns: [
    '/.next/',
    '/node_modules/',
    //'/tests/',
    '/coverage/'
  ],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
};
