export default {
  defaultTitle: 'Info Ciné',
  titleTemplate: '%s | Info Ciné',
  robotsProps:{
    nosnippet: false,
    notranslate: false,
    noimageindex: false,
    noarchive: true,
    maxSnippet: -1,
    maxImagePreview: 'standard',
    maxVideoPreview: -1,
  },
  openGraph: {
    type: 'website',
    locale: 'fr_IE',
    site_name: 'info-ciné',
  },
  twitter: {
    handle: '@handle',
    site: '@site',
    cardType: 'summary_large_image',
  },
//  facebook:{
//    appId: '1234567890'
//  }
};
