This is a [Next.js](https://nextjs.org/) project.
The goal is to have a website with SEO optimization and web performance.
This project relies on an existing API and somes external links

The website displays actors, directors and productors information and their profile details including their movies.

Location of Vercel deployment: https://infocine.vercel.app/

Key features:
- pages are generated at build time (SSG). So, all person details pages are pre-built and the page with infinite scroll are pre-built with 20 items. Revalidation has been set to 5 minutes to handle API new data event. This is good for web performance (CDN deployment).
- A sitemap has been added. It is generated at request time to avoid invalidation by API new data event. robot.txt file indicates the location of the sitemap, to help search-engine crawlers to index all our pages.
- the website uses NextSeo. It allows to set a global default SEO configuration and a local SEO configuration (per page basis). The local SEO configuration has been placed in a dedicated function to clarify the render method.
- the website uses  an infiniteScroll to diplay pages of list (home, actors, directors, productors). A custom usePerson hook has been created to handle the infinite scroll logic (and factorize it). API is provided to handle client side requests to hydrate the infinite scrolls.
- the website uses a layout (header, corpus, footer) ans some reusable components
- SASS and material-UI has been added.
- Analytics is handled by Vercel and google analytics.
- the website will adapt to render correctly on all devices.
- A theme selector has been added to the header (auto|light|dark). Auto is based on device night/day mode, user can force light or dark. User choice is maintained.
- Web vital, pages viewed and button click tracking are monitored through google analytics and google tools (page speed insight, google console, etc.).

## Small Improvements

- 404 pages due to API data missing : disabling links when data is missing (spec: broken links can appear in prod data too)
- navigation beetween menu pages : adding Links
- Correct sitemap generation : implementing firstly a cache then moving it to SSG with revalidation because request time generation was costly although cache improvement had a great impact.
- sitemap improvement : Adding lastMod, changeFreq and priority to url.
- analytics: adding google analytics tags, adding analytics session information in this readme file to specify tools for analytics
- AMP: deleting AMP for now (I need to better understand AMP implementation details in Nextjs before implementation)

Others:
- 404 pages : create a custom 404  pages with GoHome button
- adding external film API call to get cover, backdrop, genres, vote_average and vote_count to movie details (replacing broken API movie picture urls provided)

Links to Hypertext Transfer Protocol (HTTP/1.1) specifications (httpRequest, headers request/response, cache-control, content-type, ...) : https://tools.ietf.org/html/rfc7231

## Analytics

- Google Analytics has been added to the project. Members can be add to access the Google Analytics reports.
- Currently, web vital, pages viewed and some button clicks are monitored

Mesure performance, detect 404, analyse, etc. : Google Search Console  https://search.google.com/search-console/about

Test AMP validation : https://search.google.com/test/amp?hl=fr


## Getting Started with source code

Clone the git repository, then run the development server:

```bash
npm run dev
```

Then access to : http://localhost:3000/

## Environment Configuration

Environment variables can be configured in next.config.js.
You can also edit your own '.env.\[environment\]' files.
'.env.local' will always overrides the others.

## Testing

Jest, library testing and enzyme has been set up in the project.

To run the test :
```bash
npm run test
```

To run the coverage test analysis :
```bash
npm run test:coverage
```

Please note that very basic test has been implemented ;Testing implementation could be very time consuming to be accurate.
(The purpose of this website is to denote programming capacity, not to have a fully test-covered website for production)


## Deploy on Vercel

The projet is currently built and deployed on Vercel. (https://nextjs.org/docs/deployment)
Vercel auto deploy new commits to production.
Team members can be add to manage the Vercel deployment and to access analytics.

## What next

Implementation of the tests is a priority.

Implement AMP for person details pages.

Add CMS input to address missing API data to the business.

Add trailers of movies.
