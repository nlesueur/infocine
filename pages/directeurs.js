import styles from '../styles/persons.module.scss'

import { NextSeo } from 'next-seo';

import PersonCard from '../components/PersonCard.js'
import InfiniteScroll from 'react-infinite-scroll-component';

import usePersons from '../components/hooks/usePersons';


function DirecteursPage({personsResult}){

  const{items, nextOffset, fetchMoreData} = usePersons(personsResult, process.env.baseUrl + '/api/directeurs')

  function setSEO(){
    return (
      <NextSeo
        title={'Liste des directeurs'}
        description={'Dictionnaire des directeurs de production cinéma, retrouvez toute l\'information sur vos directeurs préférés'}
        openGraph={{
          title: 'InfoCiné | Liste des acteurs',
          description: 'Dictionnaire des directeurs de production cinéma, retrouvez toute l\'information sur vos directeurs préférés',
          url: process.env.baseUrl + '/directeurs',
          type:'website',
          images: [
            {
              url: process.env.baseUrl + '/images/infocine.jpg',
              width: 300,
              height: 300,
              alt: 'Picture of Info Ciné',
            },
          ]
        }}
        twitter={{
          cardType: 'summary_large_image', //twitter:card
          handle: 'info-ciné', //twitter:creator
          site: process.env.baseUrl + '/directeurs',
        }}
      />
    )
  }

  return (
    <div className="container">
      {setSEO()}

      <div className={styles.contentTitle}>
        <h1 className={styles.title}>Liste des Directeurs</h1>
      </div>

      <InfiniteScroll
        dataLength={items.length}
        next={fetchMoreData}
        hasMore={(nextOffset != null)}
        loader={<h4>Loading...</h4>}
        endMessage={
          <p style={{textAlign: 'center'}}>
            <b>Fin de la liste !</b>
          </p>
        }
      >
        {items.map((person) => (
          <PersonCard key={person.idPerson} person={person} />
        ))}
      </InfiniteScroll>
    </div>
  )
}

//Called at build time
export async function getStaticProps() {
  const apiEndPoint = process.env.directorsApiEndPoint
  const res = await fetch(apiEndPoint + '/limit/20/offset/0')
  const personsResult = await res.json()
  return { props: {personsResult}, revalidate: 5*60 }
}


export default DirecteursPage
