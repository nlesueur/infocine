import Button from '@material-ui/core/Button';


export default function Custom404() {

  return (
    <div style={{display:'flex', flexDirection:'column', alignItems:'center'}}>
      <h1><i>PAGE NOT FOUND</i></h1>
      <div>You can always check the spelling of the link, but it might be quite outdated.</div>
      <div style={{marginBottom:20}}>Or alas, the page may be gone forever...</div>


      <Button style={{marginBottom:20}}  variant="contained" color="secondary" href={'/'}>
        GO TO HOMEPAGE
      </Button>

      <img
        src={process.env.baseUrl +  '/images/mickey404.jpg'}
        alt={"Picture of 404 Mickey"}
        width={559/3}
        height={839/3}
        style={{alignItems:'center'}}
      />
    </div>
  )
}
