
var cache = require('memory-cache');

export default async (req, res) => {
  /*  This is the old dynamic sitemap generation code.

      Please see /scripts/generate-sitemap.js instead
      (called by getStaticProps of index.js with revalidation enable each 5 minutes)

      In fact, although I added cache support, I had to invalidate the bad urls
      coming from API missing data, resulting in high time consuming at request time.
      So now the sitemap generation has been moved to pre-build with revalidation.
   */

   // Using the cache if available
   let cacheSitemap = cache.get('sitemap')
   if (cacheSitemap) {
     res.setHeader('Content-Type', 'text/xml');
     res.write(cacheSitemap);
     res.end();
     return {
       props: {},
     };
   }

  //pages provenant des fiches
  const res1 = await fetch(process.env.homeApiEndPoint)
  if (res1.status !== 200) { throw new Error("Failed to fetch homeApiEndPoint") }
  const home = await res1.json()

  const res2 = await fetch(process.env.actorsApiEndPoint)
  if (res2.status !== 200) { throw new Error("Failed to fetch actorsApiEndPoint") }
  const actors = await res2.json()

  const res3 = await fetch(process.env.directorsApiEndPoint)
  if (res3.status !== 200) { throw new Error("Failed to fetch directorsApiEndPoint") }
  const directors = await res3.json()

  const res4 = await fetch(process.env.productorsApiEndPoint)
  if (res4.status !== 200) { throw new Error("Failed to fetch productorsApiEndPoint") }
  const productors = await res4.json()

  // Get the paths of pages we pre-render from the API results
  const homePaths = home.persons.map((person) => ( {personPath: person.path} ) )
  const actorPaths = home.persons.map((person) => ( {personPath: person.path} ) )
  const directorPaths = home.persons.map((person) => ( {personPath: person.path} ) )
  const productorPaths = home.persons.map((person) => ( {personPath: person.path} ) )

  //Delete duplicates
  var paths = [...new Set([...homePaths, ...actorPaths,...directorPaths, ...productorPaths])];

  //Invalidate bad paths (no API person details)
  if (process.env.env !== 'dev'){
    var i
    for (i=paths.length-1;i>=0;i--){
      const ficheId = paths[i].personPath.substring(paths[i].personPath.lastIndexOf('-') + 1);
      const res = await fetch(process.env.fichesApiEndPoint + `/${ficheId}`)
      if (res.status !== 200) {
        paths.splice(i,1)
        continue
      }
      const contentType = res.headers.get("content-type");
      if (!(contentType && contentType.indexOf("application/json") !== -1)) {
        paths.splice(i,1)
        continue
      }
      var personData = await res.json()
      if (!personData.hasOwnProperty('content')){
        paths.splice(i,1)
        continue
      }
    }
  }

  //Create the routes collection
  const routes = paths.map((path) =>  `/fiches/${path.personPath}`)
  const localRoutes = ['/','/acteurs','/directeurs','/producteurs'];
  const routesCollection = localRoutes.concat(routes);

  //Simulate a lastMod (we consider that the main change are coming from API but no data are provided in API to indicates last changes)
  let lastMod = new Date(new Date().getTime() - (24*60*1000)).toISOString().split('T')[0]

  const urlSet = routesCollection.map((route) => {
      // Build url portion of sitemap.xml
      // <lastmod>    // date format W3C i.e. AAAA-MM-JJ
      // <changefreq> // always | hourly | daily | weekly | monthly | yearly | never
      // <priority>   // 0 to 1 ; default 0.5
      return `<url><loc>${process.env.baseUrl}${route}</loc><lastmod>${lastMod}</lastmod><changefreq>daily</changefreq><priority>1</priority></url>`;
    })
    .join('');

  // Add urlSet to entire sitemap string
  const sitemap = `<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">${urlSet}</urlset>`;

  //cache the sitemap for 2 minutes
  cache.put('sitemap',sitemap,2*60*1000)

  // set response content header to xml
  res.setHeader('Content-Type', 'text/xml');
  // write the sitemap
  res.write(sitemap);
  res.end();
};
