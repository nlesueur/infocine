
//api entry point to retrieve the 20 next data from API
//invalid slug, invalid method and api not responding result in 404.

export default async function handler(req, res) {
  const { query: { offset } } = req

  if (  (req.method !== 'GET')
    ||  isNaN(parseInt(offset,10))
    ||  (offset < 0)
    ||  (offset % 20 != 0)
  ){
    res.status(404).end()
  }

  const res1 = await fetch(process.env.directorsApiEndPoint + '/limit/20/offset/' + offset)
  if (res1.status !== 200){ res.status(404).end() }
  const directorsResult = await res1.json()
  res.status(200).json(directorsResult)
}
