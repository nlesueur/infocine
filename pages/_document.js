import Document, { Html, Head, Main, NextScript } from "next/document";

import { GA_TRACKING_ID} from '../lib/gtag'

export default class MyDocument extends Document {
	render() {
		return (
			<Html>
				<Head>
          {/* favicon */}
          <link rel="icon" href="/favicon.ico" />

					{/* Global Site Tag (gtag.js) - Google Analytics */}
          <script
            async
            src={`https://www.googletagmanager.com/gtag/js?id=${GA_TRACKING_ID}`}
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${GA_TRACKING_ID}', {
              page_path: window.location.pathname,
            });
          `,
            }}
          />

				</Head>
				<body>
          {/* inject script that block rendering until light/dark theme variables based on user device has been set (avoid flash screen) */}
					<script
						dangerouslySetInnerHTML={{
							__html: blockingSetInitialColorMode,
						}}
					></script>
					<Main />
					<NextScript />
				</body>
			</Html>
		);
	}
}

// our function needs to be a string
const blockingSetInitialColorMode = `(function() {
	${setInitialColorMode.toString()}
	setInitialColorMode();
})()
`;

function setInitialColorMode() {
	function getInitialThemes() {
		//get theme specified by the user, default to auto
		var userThemePreference = window.localStorage.getItem("theme");
		if (!(typeof userThemePreference === "string")){ userThemePreference = 'auto' }

		//get theme specified by the media, default to light
		var mediaThemePreference = 'light'
		const mql = window.matchMedia("(prefers-color-scheme: dark)");
		if (typeof mql.matches === "boolean"){
			mediaThemePreference = mql.matches ? "dark" : "light"
		}

		//return it
		return {userThemePreference, mediaThemePreference}
	}

	const {userThemePreference, mediaThemePreference} = getInitialThemes();
	const root = document.documentElement;
	root.style.setProperty("--initial-user-theme-mode", userThemePreference);
	root.style.setProperty("--initial-media-theme-mode", mediaThemePreference);

	// add HTML attribute if dark mode
	if (userThemePreference == 'auto'){
		document.documentElement.setAttribute("data-theme", mediaThemePreference );
	}
	else {
		document.documentElement.setAttribute("data-theme", userThemePreference );
	}

}
