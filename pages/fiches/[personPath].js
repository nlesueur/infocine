import styles from '../../styles/fiche.module.scss'

import { useRouter } from 'next/router'

import { NextSeo } from 'next-seo';

import Head from 'next/head'

var cache = require('memory-cache');

import MovieCard from '../../components/MovieCard'

/* AMP implementation is simply removed for now */

function FichesPage({personData}){

  const {content} = personData
  const router = useRouter()

  function setSEO(){
    <NextSeo
      title={personData.title}
      description={personData.content.commentaire}
      // canonical="https://www.canonical.ie/", no canonical
      openGraph={{
        title: 'InfoCiné |' + personData.title,
        description: personData.content.description,
        url: process.env.baseUrl + router.asPath,
        type:'website',
        //-- Other type may be 'profile' or JSON-LD structure as per https://developers.google.com/search/docs/data-types/article
        //type:'profile',
        //profile: {
        //  firstName: 'First',
        //  lastName: 'Last',
        //  username: 'firstlast123',
        //  gender: 'female',
        //},
        //--
        images: [
          {
            url: process.env.imagesApiEndPoint + personData.content.photo,
            width: 300,
            height: 300,
            alt: 'Picture of ' + personData.content.nom,
          },
        ]
      }}
      twitter={{
        cardType: 'summary_large_image', //twitter:card
        handle: 'info-ciné', //twitter:creator
        site: process.env.baseUrl + router.asPath,
      }}
    />
  }



  return (
    <div>
      {setSEO()}
      <div style={{marginTop:20}}>
        {/*Card information*/}
        <div className={styles.card}>
          <div className={styles.card_img_container}>
          <img
            src={process.env.imagesApiEndPoint +  content.photo}
            alt={"Picture of " + content.nom}
            className={styles.card_img}
            importance={'high'}
          />
          </div>
          <div className={styles.card_info}>
            <h1 className={styles.h1}>{content.nom}</h1>
            <div className={styles.text_info}><b><u>Nationalité:</u></b> {content.nationalite}</div>
            <div className={styles.text_info}><b><u>date de naissance:</u></b> {content.date_naissance}</div>
            <div className={styles.text_info}><b><u>lieu de naissance:</u></b> {content.lieu_naissance}</div>
            <div className={styles.text_info}><b><u>profession:</u></b> {content.profession}</div>
            <div className={styles.text_info}>
              <b><u>dbPedia:</u> </b>
              <a className={styles.link} href={content.url_dbpedia}>{content.url_dbpedia}</a>
            </div>
            <p className={styles.text_justify}><b><u>Biographie:</u></b> {content.commentaire}</p>
          </div>
        </div>
      </div>

      <div className = {styles.titre_list_films}>
        <h2>Liste des films de {content.nom}</h2>
      </div>
      {
        Object.entries(content.movies).map(([key,value], i) => (
          <MovieCard key={i} movie={value} personName={content.nom} />
        ))
      }

    </div>
  )
  
}

// Called at build time (SSG)
export async function getStaticPaths() {

  // Call external API endpoint to get list of all persons
  const res1 = await fetch(process.env.homeApiEndPoint)
  if (res1.status !== 200) { throw new Error("Failed to fetch homeApiEndPoint") }
  const home = await res1.json()

  // Call external API endpoint to get list of actors
  const res2 = await fetch(process.env.actorsApiEndPoint)
  if (res2.status !== 200) { throw new Error("Failed to fetch actorsApiEndPoint") }
  const actors = await res2.json()

  // Call external API endpoint to get list of directors
  const res3 = await fetch(process.env.directorsApiEndPoint)
  if (res3.status !== 200) { throw new Error("Failed to fetch directorsApiEndPoint") }
  const directors = await res3.json()

  // Call external API endpoint to get list of productors
  const res4 = await fetch(process.env.productorsApiEndPoint)
  if (res4.status !== 200) { throw new Error("Failed to fetch productorsApiEndPoint") }
  const productors = await res4.json()



  // Get the paths we want to pre-render from the API results
  const homePaths = home.persons.map((person) => ( { params: {personPath: person.path} } ) )
  const actorPaths = home.persons.map((person) => ( { params: {personPath: person.path} } ) )
  const directorPaths = home.persons.map((person) => ( { params: {personPath: person.path} } ) )
  const productorPaths = home.persons.map((person) => ( { params: {personPath: person.path} } ) )

  //We can have an overlook at how data consistent is the API and send it to external tracking system if any
  //console.log('grabbing from API: ')
  //console.log('/rubrique/home: ' + homePaths.length + 'persons')
  //console.log('/rubrique/acteurs: ' + actorPaths.length + 'persons')
  //console.log('/rubrique/directeurs: ' + directorPaths.length + 'persons')
  //console.log('/rubrique/producteurs: ' + productorPaths.length + 'persons')

  //Delete duplicates
  var paths = [...new Set([...homePaths, ...actorPaths,...directorPaths, ...productorPaths])];


  //In prod, we need to delete urls that have bad API response to avoid a build error in Vercel (although if we set notFound (to gneerate a 404 response) in getStaticProps)
  //In dev, the url is generated at request time and we do not have the Vercel error, so we prefer velocity
  if (process.env.env !== 'dev'){
    var i
    for (i=paths.length-1;i>=0;i--){
      const ficheId = paths[i].params.personPath.substring(paths[i].params.personPath.lastIndexOf('-') + 1);
      const res = await fetch(process.env.fichesApiEndPoint + `/${ficheId}`)
      if (res.status !== 200) {
        console.log('missing fiche : ' + process.env.fichesApiEndPoint + `/${ficheId}`)
        paths.splice(i,1)
        continue
      }
      const contentType = res.headers.get("content-type");
      if (!(contentType && contentType.indexOf("application/json") !== -1)) {
        console.log('missing fiche : ' + process.env.fichesApiEndPoint + `/${ficheId}`)
        paths.splice(i,1)
        continue
      }
      var personData = await res.json()
      if (!personData.hasOwnProperty('content')){
        console.log('missing fiche : ' + process.env.fichesApiEndPoint + `/${ficheId}`)
        paths.splice(i,1)
        continue
      }

    }
  }

  // We will only pre-render these paths at build time. { fallback: false } means other routes should 404.
  return { paths, fallback: false }
}


// Called at build time (SSG)
export async function getStaticProps({ params }) {

  //Get the person details from the API endpoint
  var ficheId = params.personPath.substring(params.personPath.lastIndexOf('-') + 1);

  const res = await fetch(process.env.fichesApiEndPoint + `/${ficheId}`)
  if (res.status !== 200) {
    //Log if API endpoint is missing.
    //We could also send a log to an external error tracking system like Sentry, Mantisse or dedicated admin interface.
    //console.log('fiche manquante: ' + params.personPath)
    return {notFound:true}
  }
  const contentType = res.headers.get("content-type");
  if (!(contentType && contentType.indexOf("application/json") !== -1)) {
    return {notFound:true}
  }
  const personData = await res.json()
  if (!personData.hasOwnProperty('content')){
    return {notFound:true}
  }

  return { props: {personData}, revalidate : 5*60 }

}

export default FichesPage
