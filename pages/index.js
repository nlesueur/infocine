import styles from '../styles/persons.module.scss'

import { NextSeo } from 'next-seo';

import Head from 'next/head'

import PersonCard from '../components/PersonCard.js'
import InfiniteScroll from 'react-infinite-scroll-component';

import usePersons from '../components/hooks/usePersons';

import generateSitemap from '../scripts/generate-sitemap';


function Home({personsResult}){

  //We use a custom hook to manage the person items collection from the API (code factorisation)
  //We would have used a class heritage mecanism to manage the same if we were on class programmation
  const{items, nextOffset, fetchMoreData} = usePersons(personsResult, process.env.baseUrl + '/api/home')

  function setSEO(){
    return (
      <NextSeo
        title={'Accueil'}
        description={'Info ciné: Retrouvez toute l\'information et les profils des acteurs, directeurs et producteurs du cinéma'}
        openGraph={{
          title: 'InfoCiné | Accueil',
          description: 'Info ciné: Retrouvez toute l\'information et les profils des acteurs, directeurs et producteurs du cinéma',
          url: process.env.baseUrl + '/',
          type:'website',
          images: [
            {
              url: process.env.baseUrl + '/images/infocine.jpg',
              width: 300,
              height: 300,
              alt: 'Picture of Info Ciné',
            },
          ]
        }}
        twitter={{
          cardType: 'summary_large_image', //twitter:card
          handle: 'info-ciné', //twitter:creator
          site: process.env.baseUrl + '/',
        }}
      />
    )
  }

  return (
    <div className="container">
      {setSEO()}

      <div className={styles.contentTitle}>
        <h1 className={styles.title}>Bienvenue sur votre site info ciné</h1>
      </div>
      <div className={styles.intro}>
        <p>Vous trouverez ci-dessous la liste de vos acteurs, directeurs et producteurs préférés.</p>
        <p>Utilisez les rubriques du menu pour filtrer par profession et cliquez sur les cartes pour (re)découvrir leur profil et leurs films</p>
      </div>

      <InfiniteScroll
        dataLength={items.length}
        next={fetchMoreData}
        hasMore={(nextOffset != null)}
        loader={<h4>Loading...</h4>}
        endMessage={
          <p style={{textAlign: 'center'}}>
            <b>Fin de la liste !</b>
          </p>
        }
      >
        {items.map((person) => (
          <PersonCard key={person.idPerson} person={person} />
        ))}
      </InfiniteScroll>
    </div>
  )
}

//Called at build time
export async function getStaticProps() {

  //Call scripts that have to be revalidate at same time
  generateSitemap()

  const apiEndPoint = process.env.homeApiEndPoint
  const res = await fetch(apiEndPoint + '/limit/20/offset/0')
  const personsResult = await res.json()

  return { props: {personsResult}, revalidate: 5*60 }
}


export default Home
