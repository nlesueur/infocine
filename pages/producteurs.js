import styles from '../styles/persons.module.scss'

import { NextSeo } from 'next-seo';

import PersonCard from '../components/PersonCard.js'
import InfiniteScroll from 'react-infinite-scroll-component';

import usePersons from '../components/hooks/usePersons';


function ProducteursPage({personsResult}){

  const{items, nextOffset, fetchMoreData} = usePersons(personsResult, process.env.baseUrl + '/api/producteurs')

  function setSEO(){
    return (
      <NextSeo
        title={'Liste des producteurs'}
        description={'Dictionnaire des producteurs cinéma, retrouvez toute l\'information sur vos producteurs préférés'}
        openGraph={{
          title: 'InfoCiné | Liste des acteurs',
          description: 'Dictionnaire des producteurs de production cinéma, retrouvez toute l\'information sur vos producteurs préférés',
          url: process.env.baseUrl + '/producteurs',
          type:'website',
          images: [
            {
              url: process.env.baseUrl + '/images/infocine.jpg',
              width: 300,
              height: 300,
              alt: 'Picture of Info Ciné',
            },
          ]
        }}
        twitter={{
          cardType: 'summary_large_image', //twitter:card
          handle: 'info-ciné', //twitter:creator
          site: process.env.baseUrl + '/producteurs',
        }}
      />
    )
  }

  return (
    <div className="container">
      {setSEO()}

      <div className={styles.contentTitle}>
        <h1 className={styles.title}>Liste des Producteurs</h1>
      </div>

      <InfiniteScroll
        dataLength={items.length}
        next={fetchMoreData}
        hasMore={(nextOffset != null)}
        loader={<h4>Loading...</h4>}
        endMessage={
          <p style={{textAlign: 'center'}}>
            <b>Fin de la liste !</b>
          </p>
        }
      >
        {items.map((person) => (
          <PersonCard key={person.idPerson} person={person} />
        ))}
      </InfiniteScroll>
    </div>
  )
}

//Called at build time
export async function getStaticProps() {
  const apiEndPoint = process.env.productorsApiEndPoint

  const res = await fetch(apiEndPoint + '/limit/20/offset/0')
  const personsResult = await res.json()

  return { props: {personsResult}, revalidate: 5*60 }
}


export default ProducteursPage
