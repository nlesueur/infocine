import styles from '../styles/persons.module.scss'

import { NextSeo } from 'next-seo';

import PersonCard from '../components/PersonCard.js'
import InfiniteScroll from 'react-infinite-scroll-component';

import usePersons from '../components/hooks/usePersons';


function ActeursPage({personsResult}){

  //We use a custom hook to manage the person items collection from the API (code factorisation)
  //We would have used a class heritage mecanism to manage the same if we were on class programmation
  const{items, nextOffset, fetchMoreData} = usePersons(personsResult, process.env.baseUrl + '/api/acteurs')

  function setSEO(){
    return (
      <NextSeo
        title={'Liste des acteurs'}
        description={'Dictionnaire des acteurs de cinéma, retrouvez toute l\'information sur vos acteurs préférés'}
        openGraph={{
          title: 'InfoCiné | Liste des acteurs',
          description: 'Dictionnaire des acteurs de cinéma, retrouvez toute l\'information sur vos acteurs préférés',
          url: process.env.baseUrl + '/acteurs',
          type:'website',
          images: [
            {
              url: process.env.baseUrl + '/images/infocine.jpg',
              width: 300,
              height: 300,
              alt: 'Picture of Info Ciné',
            },
          ]
        }}
        twitter={{
          cardType: 'summary_large_image', //twitter:card
          handle: 'info-ciné', //twitter:creator
          site: process.env.baseUrl + '/acteurs',
        }}
      />
    )
  }

  return (
    <div className="container">
      {setSEO()}

      <div className={styles.contentTitle}>
        <h1 className={styles.title}>Liste des Acteurs</h1>
      </div>

      <InfiniteScroll
        dataLength={items.length}
        next={fetchMoreData}
        hasMore={(nextOffset != null)}
        loader={<h4>Loading...</h4>}
        endMessage={
          <p style={{textAlign: 'center'}}>
            <b>Fin de la liste !</b>
          </p>
        }
      >
        {items.map((person) => (
          <PersonCard key={person.idPerson} person={person} />
        ))}
      </InfiniteScroll>
    </div>
  )
}

//Called at build time
export async function getStaticProps() {
  const apiEndPoint = process.env.actorsApiEndPoint
  const res = await fetch(apiEndPoint + '/limit/20/offset/0')
  const personsResult = await res.json()

  return { props: {personsResult}, revalidate: 5*60 }
}


export default ActeursPage
