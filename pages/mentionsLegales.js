import styles from '../styles/mentions.module.scss'

import { NextSeo } from 'next-seo';

function MentionsPage() {

  function setSEO(){
    return (
      <NextSeo
        title={'Mentions Légales'}
        description={'Mentions Légales'}
        nofollow={true}
        noindex={true}
      />
    )
  }

  return (
    <div style={{paddingLeft:10}}>
      {setSEO()}
      <h1>Mentions légales</h1>

      <p>* Ce site n'appartient pas à une entreprise, il a été construit uniquement à titre personnel dans l'objectif de démontrer une capacité à réaliser un site.</p>
      <p>* Ce site n'enregistre pas de cookie et ne traite aucune donnée personnelle, il est uniquement consultatif.</p>
      <div style={{height:20}}/>
      <p>Ci-dessous les éléments à prendre en compte lors de la création des mentions légales pour une société d'activité commerciale:</p>
      <h2>Identification</h2>
      <p>Sur le site d'une société, on doit avoir les mentions suivantes :</p>
      <ul>
        <li>Dénomination sociale ou raison sociale</li>
        <li>Adresse du siège social</li>
        <li>Numéro de téléphone et adresse de courrier électronique</li>
        <li>Forme juridique de la société (SA, SARL, SNC, SAS, etc.)</li>
        <li>Montant du capital social</li>
        <li>Nom du directeur ou du codirecteur de la publication et celui du responsable de la rédaction s'il en existe</li>
        <li>Nom, dénomination ou raison sociale et adresse et numéro de téléphone de l'hébergeur de son site</li>
      </ul>

      <h2>Activité</h2>
      <p>Pour une activité commerciale, les mentions obligatoires sont les suivantes :</p>
      <ul>
        <li>Numéro d'inscription au registre du commerce et des sociétés</li>
        <li>Numéro individuel d'identification fiscale</li>
        <li>Conditions générales de vente (CGV) incluant le prix TTC en euros, les frais et date de livraison, les modalités de paiement, le service après vente, le droit de rétractation, la durée de l'offre, le coût de la communication à distance</li>
      </ul>

      <h2>Mentions relatives à l'utilisation de cookies</h2>
      <p>Un cookie est un petit fichier informatique, un traceur. Il permet d'analyser le comportement des usagers lors de la visite d'un site internet, de la lecture d'un courrier électronique, de l'installation ou de l'utilisation d'un logiciel ou d'une application mobile.</p>
      <p>Les éditeurs de sites ou d'applications qui utilisent des cookies doivent :</p>
      <ul>
        <li>informer les internautes de la finalité des cookies,</li>
        <li>obtenir leur consentement,</li>
        <li>fournir aux internautes un moyen de les refuser.</li>
      </ul>
      <p>La durée de validité du consentement donné dans ce cadre est de 13 mois maximum.</p>

      <h2>Mentions relatives à l'utilisation de données personnelles</h2>
      <p>Les sites qui utilisent des données personnelles doivent obligatoires mentionner les informations suivantes :</p>
      <ul>
        <li>Coordonnées du délégué à la protection des données (DPO ou DPD) de l'organisme, s'il a été désigné, ou d'un point de contact sur les questions de protection des données personnelles</li>
        <li>Finalité poursuivie par le traitement auquel les données sont destinées</li>
        <li>Caractère obligatoire ou facultatif des réponses et conséquences éventuelles à l'égard de l'internaute d'un défaut de réponse</li>
        <li>Destinataires ou catégories de destinataires des données</li>
        <li>Droits d'opposition, d'interrogation, d'accès et de rectification</li>
        <li>Au besoin, les transferts de données à caractère personnel envisagés à destination d'un État n'appartenant pas à l'Union européenne</li>
        <li>Base juridique du traitement de données (c'est-à-dire ce qui autorise légalement le traitement : il peut s'agir du consentement des personnes concernées, du respect d'une obligation prévue par un texte, de l'exécution d'un contrat notamment)</li>
        <li>Mention du droit d'introduire une réclamation (plainte) auprès de la Cnil</li>
      </ul>

      <p>Attention : l'absence d'une information obligatoire est punie d'une amende de 1 500 €. Tout traitement informatique non consenti des données recueillies est puni de 5 ans d'emprisonnement et de 300 000 € d'amende.</p>
      <p>Seules les plaintes relatives au commerce électronique peuvent être traitées par la DGCCRF.</p>
    </div>
  )
}

export default MentionsPage
