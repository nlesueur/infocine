import React from 'react';
import { render, screen } from '@testing-library/react';

//import { MemoryRouter } from "react-router-dom";
import Home from '../../pages/index'

test('Page Home is rendering', () => {

  let indexData = require('./index.test.json');

  const useRouter = jest.spyOn(require('next/router'), 'useRouter');
  useRouter.mockImplementation(() => ({
    pathname: '/',
  }));

  render(
    <Home personsResult={indexData}/>
  );

  expect(screen.getAllByText("François Damiens")[0]).toBeInTheDocument();

});
