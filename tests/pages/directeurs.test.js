import React from 'react';
import { render, screen } from '@testing-library/react';

//import { MemoryRouter } from "react-router-dom";
import DirecteursPage from '../../pages/directeurs'

test('Page Directeurs is rendering', () => {

  let directorsData = require('./directeurs.test.json');

  const useRouter = jest.spyOn(require('next/router'), 'useRouter');
  useRouter.mockImplementation(() => ({
    pathname: '/',
  }));

  render(
    <DirecteursPage personsResult={directorsData}/>
  );

  expect(screen.getAllByText("Lars von Trier")[0]).toBeInTheDocument();

});
