import React from 'react';
import { render, screen } from '@testing-library/react';

//import { MemoryRouter } from "react-router-dom";
import ActeursPage from '../../pages/acteurs'

test('Page Acteurs is rendering', () => {

  let actorsData = require('./acteurs.test.json');

  const useRouter = jest.spyOn(require('next/router'), 'useRouter');
  useRouter.mockImplementation(() => ({
    pathname: '/',
  }));

  render(
    <ActeursPage personsResult={actorsData}/>
  );

  expect(screen.getAllByText("Zabou Breitman")[0]).toBeInTheDocument();

});
