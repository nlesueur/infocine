import React from 'react';
import { render, screen } from '@testing-library/react';

//import { MemoryRouter } from "react-router-dom";
import ProducteursPage from '../../pages/producteurs'

test('Page Producteurs is rendering', () => {

  let productorsData = require('./producteurs.test.json');

  const useRouter = jest.spyOn(require('next/router'), 'useRouter');
  useRouter.mockImplementation(() => ({
    pathname: '/',
  }));

  render(
    <ProducteursPage personsResult={productorsData}/>
  );

  expect(screen.getAllByText("Alain Cavalier")[0]).toBeInTheDocument();

});
