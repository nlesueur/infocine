import React from 'react';
import { render, screen } from '@testing-library/react';

//import { MemoryRouter } from "react-router-dom";
import FichesPage from '../../../pages/fiches/[personPath]'

test('Page fiche is rendering', () => {

  const useRouter = jest.spyOn(require('next/router'), 'useRouter');
  useRouter.mockImplementation(() => ({
    pathname: '/',
  }));

  let personData = require('./[personPath].test.json');

  render(
    <FichesPage personData={personData}/>
  );
  expect(screen.getAllByText("Liam Neeson")[0]).toBeInTheDocument();
  expect(screen.getAllByText("Le Prophète")[0]).toBeInTheDocument();
});
